(define (subsets s)
  (if (null? s)
      (list '())
      (let ((rest (subsets (cdr s))))
        (append rest (map (lambda (x) (cons (car s) x))
                          rest)))))

(subsets (list 1 2 3))

(subsets '())

(subsets '(1))

(subsets '(2 1))

(subsets '(3 2 1))
