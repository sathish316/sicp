(define (sqrt-iter guess x)
  (if (good-enough? guess x)
      guess
      (sqrt-iter (improve guess x) x)))

(define (improve guess x)
  (average guess (/ x guess)))

(define (average x y)
  (/ (+ x y) 2))

(define tolerance 0.001)

(define (good-enough? guess x)
  (< (abs (- (square guess) x)) tolerance))

(define (sqrt x)
  (sqrt-iter 1.0 x))

(sqrt 2)

(sqrt 25)

(sqrt 0.000009)

(sqrt 999999999999998)
