(define (timed-prime-test n)
  (start-prime-test n (runtime)))

(define (start-prime-test n start-time)
  (if (fast-prime? n 100)
      (report-prime n (- (runtime) start-time))
      false))

(define (report-prime n elapsed-time)
  (newline)
  (display n)
  (newline)
  (display elapsed-time)
  true)

(timed-prime-test 23)

(timed-prime-test 25)

(define (search-for-primes start end num)
  (cond ((> start end) (display "Done"))
        ((= num 0) (display "Done"))
        ((timed-prime-test start) (search-for-primes (+ start 1) end (- num 1)))
        (else (search-for-primes (+ start 1) end num))))

(search-for-primes 50 100 3)

(search-for-primes 1000 10000 3)
; 0.3 using expmod3 fermat test

(search-for-primes 10000 100000 3)
; 0.61 using expmod2 fermat-test
; 3.5 using expmod3 fermat test

(define (exp b e)
  (if (= e 0)
      1
      (* b (exp b (- e 1)))))

(search-for-primes (exp 10 9) (exp 10 10) 3)

                                        ; 0.07 with basic prime?
                                        ; 0.04 with improved smallest divisor
                                        ; 0.01 with fast-prime?

(search-for-primes (exp 10 10) (exp 10 11) 3)

                                        ; 0.20 with basic prime?
                                        ; 0.12 with improved smallest prime
                                        ; 0.02 with fast-prime?

(search-for-primes (exp 10 13) (exp 10 14) 3)
                                        ; 0.02 with fast-prime

